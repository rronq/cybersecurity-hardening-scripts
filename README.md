# CyberSecurity Hardening Scripts

These are just a few scripts made for hardening a Windows or Linux system. They're very basic and are intended to be used for learning purposes. Students should also be encouraged to expand and create their own scripts.